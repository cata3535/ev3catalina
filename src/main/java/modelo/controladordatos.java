/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;


import ciisa.dao.PersonaJpaController;
import ciisa.dao.exceptions.NonexistentEntityException;
import ciisa.unidad2.entity.Persona;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Jonathan
 */
@WebServlet(name = "controladordatos", urlPatterns = {"/controladordatos"})
public class controladordatos extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet controladordatos</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet controladordatos at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String boton=request.getParameter("accion");
        PersonaJpaController dao = new PersonaJpaController();
        List<Persona> listaPersona=null;
        
        if (boton.equals("ingreso")) {

            request.getRequestDispatcher("ingreso.jsp").forward(request, response);

        }
        
        if (boton.equals("ingresar")) {

            String rut = request.getParameter("rut");
            String nombre = request.getParameter("nombre");
            String apellidop = request.getParameter("apellidop");
            String apellidom = request.getParameter("apellidom");
            String email = request.getParameter("email");
            
            Persona persona=new Persona();
            persona.setRut(rut);
            persona.setNombre(nombre);
            persona.setApellidoPaterno(apellidop);
            persona.setApellidoMaterno(apellidom);
            persona.setEmail(email);
            try {
                dao.create(persona);
            } catch (Exception ex) {
                Logger.getLogger(controladordatos.class.getName()).log(Level.SEVERE, null, ex);
            }
            listaPersona = dao.findPersonaEntities();

            request.setAttribute("listaPersona", listaPersona);
            request.getRequestDispatcher("index.jsp").forward(request, response);
        }
        if (boton.equals("consulta")){
        
            listaPersona = dao.findPersonaEntities();
            
            request.setAttribute("listaPersona", listaPersona);
            request.getRequestDispatcher("consultar.jsp").forward(request, response);
        
       }
        if (boton.equals("borrar")) {
             
            String elementoSeleccionado = request.getParameter("seleccion");
          
            try {
                dao.destroy(elementoSeleccionado);
            } catch (NonexistentEntityException ex) {
               Logger.getLogger(controladordatos.class.getName()).log(Level.SEVERE, null, ex);
               
            }
              listaPersona = dao.findPersonaEntities();

            request.setAttribute("listaPersona", listaPersona);
            request.getRequestDispatcher("consultar.jsp").forward(request, response); 
              
         }
        if (boton.equals("editar")) {
            
            String elementoSeleccionado = request.getParameter("seleccion");
            
            Persona buscar = dao.findPersona(elementoSeleccionado);
            
            request.setAttribute("buscar", buscar);
            
            request.getRequestDispatcher("editar.jsp").forward(request, response);

        }
         if (boton.equals("cancelar")) {
            listaPersona = dao.findPersonaEntities();            
            request.setAttribute("listaPersona", listaPersona);
            request.getRequestDispatcher("consultar.jsp").forward(request, response);

        }
         if (boton.equals("aplicar")) {
            
            String rut = request.getParameter("rut");
            String nombre = request.getParameter("nombre");
            String apellidop = request.getParameter("apellidop");
            String apellidom = request.getParameter("apellidom");
            String email = request.getParameter("email");
            
            Persona persona=new Persona();
            persona.setRut(rut);
            persona.setNombre(nombre);
            persona.setApellidoPaterno(apellidop);
            persona.setApellidoMaterno(apellidom);
            persona.setEmail(email);
            
            try {
                dao.edit(persona);
            } catch (Exception ex) {
                Logger.getLogger(controladordatos.class.getName()).log(Level.SEVERE, null, ex);
            }
            listaPersona = dao.findPersonaEntities();

            request.setAttribute("listaPersona", listaPersona);
            request.getRequestDispatcher("consultar.jsp").forward(request, response);

        } 
        if (boton.equals("inicio")) {
            
            request.getRequestDispatcher("index.jsp").forward(request, response);

        } 
      
    
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
