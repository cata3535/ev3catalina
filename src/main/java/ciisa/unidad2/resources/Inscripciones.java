/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciisa.unidad2.resources;

import ciisa.dao.PersonaJpaController;
import ciisa.dao.exceptions.NonexistentEntityException;
import ciisa.unidad2.entity.Persona;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Jonathan
 */
@Path("registros")
public class Inscripciones {
    
    @GET
    @Produces (MediaType.APPLICATION_JSON)
    public Response buscarregistros(){
        PersonaJpaController dao= new PersonaJpaController();
        List<Persona> lista =dao.findPersonaEntities();
        
        return Response.ok(200).entity(lista).build();
    }
    
    @POST
    @Produces (MediaType.APPLICATION_JSON)
    public Response ingreso(Persona persona){
        try {
            PersonaJpaController dao= new PersonaJpaController();
            dao.create(persona);
        } catch (Exception ex) {
            Logger.getLogger(Inscripciones.class.getName()).log(Level.SEVERE, null, ex);
        }
          return Response.ok(200).entity(persona).build();
    }
    
    @DELETE
    @Path("/{iddelete}")
    @Produces (MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("iddelete") String iddelete){
        try {
            PersonaJpaController dao= new PersonaJpaController();
            dao.destroy(iddelete);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(Inscripciones.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok("Registro Eliminado").build();
    }
    
    @PUT
    @Produces (MediaType.APPLICATION_JSON)
    public Response update(Persona persona){
    try {
            PersonaJpaController dao= new PersonaJpaController();
            dao.edit(persona);
        } catch (Exception ex) {
            Logger.getLogger(Inscripciones.class.getName()).log(Level.SEVERE, null, ex);
        }
          return Response.ok(200).entity(persona).build();
        
    }
}
